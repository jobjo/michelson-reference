#!/usr/bin/env python

from jinja2 import Environment, FileSystemLoader, select_autoescape, Markup, StrictUndefined
from language_def import LanguageDefinition

import re

import argparse

from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import HtmlFormatter
from pygments_michelson import MichelsonLexer

from bs4 import BeautifulSoup

from restructured_text import publish_parts_exn

try_michelson_url = "https://try-michelson.tzalpha.net"

def keep_lines(s, ranges):
    if ranges == "" or ranges == "-" :
        return s
    ranges_lst = [x.split("-") for x in ranges.split(",")]
    lines = s.splitlines(keepends=True)
    res = ""
    for r in ranges_lst:
        if len(r) == 1:
            res += lines[int(r[0])]
        elif len(r) == 2:
            if r[0] == '':
                start = 0
            else:
                start = int(r[0])
            if r[1] == '':
                stop = len(lines)
            else:
                stop = int(r[1])+1
            res += "".join(lines[start:stop])
        else:
            res = f'Invalid Range {ranges}'
    return res

# generates the documentation
env = Environment(
    loader=FileSystemLoader('templates'),
    autoescape=select_autoescape(['html', 'xml']),
    undefined=StrictUndefined
)

def main():
    parser = argparse.ArgumentParser(description='Generates michelson instruction reference from ott.')
    parser.add_argument(
        '--strict', dest='strict', action='store_const',
        const=True, default=False,
        help='terminate on undocumented instructions')
    parser.add_argument(
        '--standalone', dest='template_file', action='store_const',
        const='body.html', default='index.html',
        help='generate standalone reference: intended for embedding in michelson documentation')
    parser.add_argument(
        '--example-path',
        dest='example_path',
        type=str,
        default='../../../src/bin_client/test/contracts/',
        help='path to contracts folder, e.g. tezos-node/src/bin_client/test/contracts/')
    parser.add_argument(
        '--output',
        dest='output',
        type=argparse.FileType('w+'),
        default='docs/index.html',
        help='path to output file')
    
    args = parser.parse_args()

    lang_def = LanguageDefinition(
        strict=args.strict,
        example_path=args.example_path
    )

    def link_html(h):
        soup = BeautifulSoup(h, features="html.parser")
        for literal in soup.find_all('tt'):
            suffix = literal.text.split(' ')[0]
            prefix = None
            if lang_def.is_instruction(suffix):
                prefix = 'instr'
            elif lang_def.is_type(suffix):
                prefix = 'type'

            if prefix is not None:
                tag = soup.new_tag('a',
                                   href=f"#{prefix}-{suffix}",
                                   _class="generated-interlink")
                literal.wrap(tag)
        return soup

    def rst_filter(s):
        rst_html = publish_parts_exn(source=s, writer_name='html')['body']
        return Markup(link_html(rst_html))

    def remove_newline_filter(s):
        """A ninja filter that removes newlines."""
        return s.replace("\n", " ")

    def rst_inline_filter(s):
        if s == '':
            return Markup('')
        s = publish_parts_exn(source=s, writer_name='html')['body']
        m = re.match(r'<p>(.*)</p>', s)
        return Markup(link_html(m.group(1)))

    def pp_michelson_filter(code):
        return Markup(highlight(code, MichelsonLexer(), HtmlFormatter()))

    env.filters['rst'] = rst_filter
    env.filters['rst_inline'] = rst_inline_filter
    env.filters['pp_michelson'] = pp_michelson_filter
    env.filters['keep_lines'] = keep_lines
    env.filters['remove_newline'] = remove_newline_filter

    template = env.get_template(args.template_file)
    args.output.write(template.render(
        pygments_css=HtmlFormatter().get_style_defs('.highlight'),
        lang_def=lang_def,
        try_michelson_url=try_michelson_url
    ))


if __name__ == '__main__':
    main()
