tezos_client=tezos-client
ott?=ott
TEZOS_HOME?=~/dev/tezos/
TEZOS_PROTOCOL_VERSION?=008
TEZOS_CONTRACTS?=$(TEZOS_HOME)/tests_python/contracts_$(TEZOS_PROTOCOL_VERSION)
TEZOS_CLIENT?=$(TEZOS_HOME)/tezos-client

all: site

%.json: %.ott
	$(ott) -i $< -o $@

michelson_embed.tex: michelson.ott
	$(ott) -tex_wrap false -coq_expand_list_types false -i $< -o $@

michelson.tex: michelson.ott
	$(ott) -coq_expand_list_types false -i $< -o $@

michelson.html: michelson.tex
	hevea michelson.tex

DOC_DEPS=generate.py michelson.json michelson-meta.yaml generate.py $(shell find templates -iname \*.html)

templates/rules: michelson_embed.tex
ifeq (, $(shell which hevea))
	$(error "No hevea in $$PATH, see README.org")
endif
	mkdir -p $@
	poetry run python pp_latex_rules.py

michelson.merged.json:
	poetry run python generate_meta.py $@

docs/index.html: ${DOC_DEPS} $(shell find static -iname \*.js -or -iname \*.css) michelson_embed.tex templates/rules michelson.merged.json
	mkdir -p docs/static/
	cp -rv static/* docs/static/
	cp michelson.merged.json docs/michelson.json
	TEZOS_PROTOCOL_HASH=$(TEZOS_PROTOCOL_HASH) TEZOS_PROTOCOL_VERSION=$(TEZOS_PROTOCOL_VERSION) poetry run python generate.py --example-path $(TEZOS_CONTRACTS) --output $@

site: docs/index.html

docs/michelson_reference.html: ${DOC_DEPS}
	poetry run python generate.py --standalone > $@

clean:
	rm -rf docs/* michelson.tex templates/rules michelson.html
