
// returns r such that if q * y + r = x, and 0 <= r < abs(y)
function mod(x, y) {
    if (x < 0) {
        return y - -(x % y);
    } else {
        return x % y;
    }
}

jQuery(function ($) {
    var kcEnter = 13;
    var kcEscape = 27;
    var kcSlash = 47;

    var toc = $('#toc');
    var els = toc.find('.type, .instr');
    var search = toc.find('#toc-search');

    var oldValue = false;
    var links = false;
    var idx = false;

    // returns r such that if q * y + r = x, and 0 <= r < abs(y)
    function mod(x, y) {
        if (x < 0) {
            return x - (x % y);
        } else {
            return x % y;
        }
    }

    search.on("keyup", function(e) {
        if (e.keyCode == kcEscape) {
            search.blur();
        }

        if (e.keyCode == kcEnter) {
            var offset = e.shiftKey ? -1 : 1;

            // hide all markers
            toc.find('a').removeClass('marked');

            var allLinks = {};
            toc.find('a').filter(':visible').each(function () {
                var href = $(this).attr('href');
                if (!(href in allLinks)) {
                    allLinks[href] = $(this)
                }
            });
            links = allLinks;
            length = Object.keys(links).length;
            if (links) {
                if (idx === false) {
                    idx = mod((e.shiftKey ? -1 : 0), length);
                }

                var gotoLink = Object.keys(links)[idx];
                links[gotoLink].addClass('marked');

                if (gotoLink) {
                    location.href = gotoLink;
                    search.focus()
                    idx = mod((idx + offset), length);
                }
            }
            return true;
        }

        var value = $(this).val();
        var caseSensitiveMatch = (value !== value.toLowerCase());
        value = value.replace('--', '—');

        if (value !== oldValue) {
            links = false;
            idx = false;
        }

        if (value != '') {
            toc.find('details').hide();
            toc.find('details').attr('open', 'closed');

            els.each(function() {
                var match = caseSensitiveMatch ?
                    ($(this).text().indexOf(value) > -1) :
                    ($(this).text().toLowerCase().indexOf(value.toLowerCase()) > -1);

                $(this).toggle(match);
                if (match) {
                    $(this).parents('details').attr('open', 'open');
                    $(this).parents('details').show();
                }
            });
        } else {
            els.toggle(true);
            toc.find('details').removeAttr('open');
            toc.find('details').show();
        }

        oldValue = value;
    });
    search.keyup();

    $('body').on('keypress', function (e) {
        if (e.keyCode == kcSlash) {
            search.focus().select();
            e.preventDefault()
        }
    })
});
