import os

# The testing framework needs to know the location of a tezos-client
# and the set of example/test contracts distributed in the tezos
# source distribution. This can be set by setting the environment
# variable TEZOS_HOME to a Tezos source distribution containing said
# binary and test contracts. See variables below if the two paths
# needs to be set separately.
TEZOS_HOME = os.environ.get("TEZOS_HOME")
# This must be kept in sync with ../../michelson_reference/Makefile
TEZOS_PROTOCOL_VERSION = "008"
TEZOS_PROTOCOL_HASH = "PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA"

# TEZOS_CLIENT should point to a tezos-client binary. It defaults to
# TEZOS_home/tezos-client, but can be customized through the
# TEZOS_CLIENT environment variable.
TEZOS_CLIENT = (os.environ.get("TEZOS_CLIENT") or
                TEZOS_HOME and f'{TEZOS_HOME}/tezos-client')
assert TEZOS_CLIENT is not None, \
    "Could not find tezos-client (set TEZOS_HOME or TEZOS_CLIENT)"

# TEZOS_CONTRACTS should point to the set of example/test contracts
# distributed in the tezos source distribution. It defaults to
# TEZOS_HOME/tests_python/contracts_TEZOS_PROTOCOL_VERSION, but can be
# customized through the TEZOS_CONTRACTS environment variable.
TEZOS_CONTRACTS = \
  (os.environ.get("TEZOS_CONTRACTS") or
   TEZOS_HOME and
   f'{TEZOS_HOME}/tests_python/contracts_{TEZOS_PROTOCOL_VERSION}')
assert TEZOS_CONTRACTS is not None, \
    ("Could not find Tezos contract test suite  (set TEZOS_HOME "
     "or TEZOS_CONTRACTS)")
