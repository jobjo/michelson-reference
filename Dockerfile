FROM ocaml/opam2 as builder

WORKDIR /tempDir
RUN sudo chown -R opam: /tempDir

RUN DEBIAN_FRONTEND=noninteractive sudo apt-get update && sudo apt-get install -y -q m4 make python3 python3-pip python3-venv

# Add opam dependencies
RUN opam update; \
    opam install -y hevea; \
    opam pin add ott https://gitlab.com/nomadic-labs/ott.git#json

### Begin Python setup
# Required to have poetry in the path in the CI
ENV PATH="/home/opam/.local/bin:${PATH}"

# Copy poetry files to install the dependencies in the docker image
COPY poetry.lock poetry.lock
COPY pyproject.toml pyproject.toml

# Install poetry (https://github.com/python-poetry/poetry) and project dependencies
RUN pip3 install --user poetry==1.0.10;\
    poetry install
### End Python setup

# Check out tezos
RUN git clone --depth 1 https://gitlab.com/tezos/tezos.git
ENV TEZOS_HOME /tempDir/tezos

# Start build
COPY --chown=opam . /tempDir

RUN eval $(opam env); \
    make check-committed; \
    make -C michelson_reference docs/index.html

FROM nginx:alpine
COPY --from=builder /tempDir/michelson_reference/docs/ /usr/share/nginx/html/
